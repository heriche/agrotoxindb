## agrotoxinDB

An R API to the agroToxin database using R6 objects.

Usage:

``` R
library(agrotoxinDB)

dbc <- DBConnection$new(host = "localhost", user = "anonymous", password = "")
compound.handle <- CompoundHandle$new(dbc)
compounds <- compound.handle$get_all_compounds()
```

### How to recreate the database locally
The instructions below are for Debian-based Linux systems (e.g. Ubuntu).
Note that you need admin privileges on the machine.
1. Install a MariaDB server

``` bash
sudo apt update
sudo apt install mariadb-server mariadb-client -y
```
2. Configure the server

``` bash
sudo mysql_secure_installation
```
This will walk you through some prompts to secure the server.  
Since it should be a fresh installation, simply press ENTER at the first prompt   
then N when asked to set up a root password and finally Y for all subsequent ones.  

3. Set up an administrative user

``` bash
sudo mariadb
```
Then at the MariaDB prompt:

``` sql
 GRANT ALL ON *.* TO 'admin'@'localhost' IDENTIFIED BY '<password>' WITH GRANT OPTION;
 FLUSH PRIVILEGES;
 \q
```
4. Create the database  
Download the database dump file (agrotoxin_database.sql) from the web site to the current working directory.  
Log back in using the admin account:

``` bash
mariadb -u admin -p
```
Enter the password you selected above then at the MariaDB prompt:

``` sql
SOURCE agrotoxin_database.sql
\q
```
5. Set up a passwordless account for querying the database.  
Log back in using the admin account:

``` bash
mariadb -u admin -p
```
Enter the password you selected above then at the MariaDB prompt:

``` sql
CREATE USER 'anonymous'@'%';
GRANT SELECT ON `agroTox%`.* TO 'anonymous'@'%';
FLUSH PRIVILEGES;
\q
```


